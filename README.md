    git init : Cette commande initialise un nouveau dépôt Git dans le répertoire local. Cela crée un dossier .git qui contient tous les fichiers nécessaires pour le suivi des versions.
        Utilisation : git init

    git clone : La commande git clone est utilisée pour créer une copie locale d'un dépôt distant.
        Utilisation : git clone <URL_du_dépôt>

    git add : Cette commande ajoute des modifications à l'index (zone de staging) pour les inclure dans le prochain commit.
        Utilisation : git add <nom_du_fichier> pour ajouter un fichier spécifique ou git add . pour ajouter tous les fichiers modifiés.

    git commit : La commande git commit enregistre les modifications indexées dans le dépôt local.
        Utilisation : git commit -m "Message de commit"

    git push : Cette commande envoie les commits locaux vers le dépôt distant.
        Utilisation : git push origin <nom_de_branche>

    git pull : La commande git pull récupère les modifications depuis le dépôt distant et les fusionne dans la branche actuelle.
        Utilisation : git pull origin <nom_de_branche>